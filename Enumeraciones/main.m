//
//  main.m
//  Enumeraciones
//
//  Created by Fernando Rodríguez Romero on 20/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        
        // NSString
        [@"Hola\nMundo" enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
            static int i = 0;
            NSLog(@"Linea %d: %@", i++, line);
            
        }];
        
        
        // NSArray
        NSArray *nums = @[@1, @2, @3, @4, @5.5, @6.33333324, @(6.3 + 1.2),[NSNumber numberWithFloat:8.8]];
        
        [nums enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            NSLog(@"El objeto %lu es %@",(unsigned long) idx, obj);
            
            if ([obj intValue] >= 5) {
                NSLog(@"Nos pasamos");
                *stop = YES;
            }
        }];
        
        // Mira mamá, ¡en paralelo!
        [nums enumerateObjectsWithOptions:NSEnumerationConcurrent
                               usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                   NSLog(@"El objeto %lu: %@", idx, obj);
                               }];
        
        
        // Dictionary
        NSDictionary *bso = @{@"Zimmer" : @[@"Inception", @"Interstellar"],
                              @"Barry" : @[@"Out of Africa", @"Born Free"]};
        
        
        [bso enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSLog(@"%@ ha escrito las siguientes BSO: %@", key, obj);
        }];
        
        
    }
    return 0;
}
